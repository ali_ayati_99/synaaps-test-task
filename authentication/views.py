from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser

from .models import User, Profile
from .serializers import UserSerializer, ProfileSerializer

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    List and retrieve user instances for admin users. Use admin panel for CUD.
    """
    permission_classes = [IsAdminUser, ]
    serializer_class = UserSerializer
    
    def get_queryset(self):
        return User.objects.all()


class ProfileViewSet(viewsets.ReadOnlyModelViewSet):
    """
    List and retrieve profile instances for admin users. Use admin panel for CUD.
    """
    permission_classes = [IsAdminUser, ]
    serializer_class = ProfileSerializer
    
    def get_queryset(self):
        return Profile.objects.all()
