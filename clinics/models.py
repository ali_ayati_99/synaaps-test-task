from django.db import models

from patients.models import Patient


class PolyClinic(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self) -> str:
        return self.name


class Clinic(models.Model):
    name = models.CharField(max_length=128)
    polyclinic = models.ForeignKey(to=PolyClinic, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f"{self.name} from {self.polyclinic}"


class Admission(models.Model):
    patient = models.ForeignKey(to=Patient, on_delete=models.CASCADE)
    clinic = models.ForeignKey(to=Clinic, on_delete=models.CASCADE)
    insurance_type = models.CharField(max_length=128)
    insurance_number = models.CharField(max_length=128)
    complaint = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return f"{self.patient} from {self.clinic}"


class AdmissionStatus(models.Model):
    class Status(models.IntegerChoices):
        WAITING = 0, "Wating"
        ENTERED = 1, "Entered" 
        DISMISSED = 2, "Dismissed"
    admission = models.ForeignKey(to=Admission, on_delete=models.CASCADE)
    status = models.IntegerField(choices=Status.choices, default=Status.WAITING)
    timestamp = models.DateTimeField(auto_now_add=True)

    @property
    def readable_status(self):
        return self.Status.choices[self.status][1]

    def __str__(self) -> str:
        return f"{self.admission}: {self.readable_status}"


class SharedPatient(models.Model):
    patient = models.ForeignKey(to=Patient, on_delete=models.CASCADE)
    clinic = models.ForeignKey(to=Clinic, on_delete=models.CASCADE)
