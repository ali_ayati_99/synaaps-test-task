from django.apps import AppConfig


class ClinicsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'clinics'

    def ready(self) -> None:
        import clinics.signals
        return super().ready()
