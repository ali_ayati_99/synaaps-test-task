from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Admission, AdmissionStatus


@receiver(post_save, sender=Admission)
def create_status(sender, instance, created, **kwargs):
    if created:
        AdmissionStatus.objects.create(admission=instance)
